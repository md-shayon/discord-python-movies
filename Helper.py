import re

class Helper():
    def get_movie_name(self, msg):
        try:
            pattern = "(?<=\/movie\s).+"
            reg_exp = re.compile(pattern)
            movie_name_groups = reg_exp.search(msg)
            movie_name = movie_name_groups.group(0)
            return movie_name
        except Exception as e:
            print(e)

        return None

    def format_movie_name_with_dash(self, movie_name):
        try:
            no_special_char_movie, num = re.subn("[\'\.\:]", '', movie_name)
            movie_word_list = no_special_char_movie.split(' ')
            new_movie_str = ""
            for i in range(0, len(movie_word_list)):
                if movie_word_list[i] != '':
                    if i != len(movie_word_list) - 1:
                        new_movie_str += f"{movie_word_list[i]}-"
                    else:
                        new_movie_str += movie_word_list[i]
            # print(new_movie_str)
            return new_movie_str
        except Exception as e:
            print(e)
        return movie_name

    def format_movie_keyword_with_plus(self, movie_name):
        try:
            replace_colons = movie_name.replace(":", "%3A").replace("'", "%27")
            movie_word_list = replace_colons.split(' ')
            new_movie_str = ""
            for i in range(0, len(movie_word_list)):
                if movie_word_list[i] != '':
                    if i != len(movie_word_list) - 1:
                        new_movie_str += f"{movie_word_list[i]}+"
                    else:
                        new_movie_str += movie_word_list[i]
            # print(new_movie_str)
            return new_movie_str
        except Exception as e:
            print(e)
        return movie_name

    def movie_embed_element(self, content, discord):
        try:
            embed = discord.Embed(title=content['title'], description=content["detail"], color=0x00ff00)
            embed.set_thumbnail(url=content['thumbnail'])
            embed.add_field(name=" _", value="Likes: {0} Dislikes: {1}".format(content['likes'], content['dislikes']),
                            inline=False)
            embed.add_field(name=" _", value="Release: {0} Genre: {1}".format(content['release'], content['genre']),
                            inline=False)

            return embed
        except Exception as e:
            print(e)

        return None

    def movie_embed_nf_element(self, discord):
        try:
            embed = discord.Embed(title='Not found', description="Make sure the spelling of the movie name is correct", color=0x00ff00)
            return embed
        except Exception as e:
            print(e)

        return None