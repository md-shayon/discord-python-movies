# Example - https://discordpy.readthedocs.io/en/stable/quickstart.html
# tutorial - https://www.youtube.com/watch?v=SPTfmiYiuok
import discord
import os
import json
import requests
import random
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

db = {
    "encouragements": []
}

intents = discord.Intents.default()
intents.message_content = True

# Connection to discord
client = discord.Client(intents=intents)

sad_words = ["sad", 'depressed', "unhappy", "angry", "miserable", "depressing"]
starter_encouragement = [
    "Cheer up!",
    "Hang in there.",
    "You are a great person / bot"
]
if "responding" not in db.keys():
    db["responding"] = True


def get_quotes():
    response = requests.get("https://zenquotes.io/api/random")
    json_data = json.loads(response.text)
    quote = json_data[0]['q'] + " - " + json_data[0]["a"]
    return quote


def update_encouragements(encouraging_message):
    if "encouragements" in db.keys():
        encouragements = db["encouragements"]
        encouragements.append(encouraging_message)
        db["encouragements"] = encouragements
    else:
        db["encouragements"] = [encouraging_message]

def delete_encouragement(index):
    encouragements = db["encouragements"]
    if len(encouragements) > index:
        del encouragements[index]
        db["encouragements"] = encouragements




# register an event
@client.event
async def on_ready():
    print("We have logged in as {0.user}".format(client))


# all the message we receive
@client.event
async def on_message(message):
    # print(message)
    # We do not want to do anything if the message is from bot itself
    if message.author == client.user:
        return

    msg = message.content

    # Commands from the bot
    if msg.startswith("$hi"):
        await message.channel.send("Hello")

    if msg.startswith("$inspire"):
        quote = get_quotes()
        await message.channel.send(quote)

    if db['responding']:
        options = starter_encouragement
        if "encouragements" in db.keys():
            options= options + db["encouragements"]

        if any(word in msg for word in sad_words):
            await message.channel.send(random.choice(options))

    if msg.startswith("$new"):
        encouraging_message = msg.split("$new ", 1)[1]
        update_encouragements(encouraging_message)
        await message.channel.send("New encouraging message added!")

    if msg.startswith("$del"):
        encouragements = []
        if "encouragements" in db.keys():
            index = int(msg.split("$del", 1)[1])
            delete_encouragement(index)
            encouragements = db["encouragements"]
        await message.channel.send(encouragements)

    if msg.startswith("$list"):
        encouragements = []
        if "encouragements" in db.keys():
            encouragements = db["encouragements"]
        await message.channel.send(encouragements)

    if msg.startswith("$responding"):
        value = msg.split("$responding ", 1)[1]
        if value.lower() == "true":
            db["responding"] = True
            await message.channel.send("Responding is on.")
        else:
            db["responding"] = False
            await message.channel.send("Responding is off.")


client.run(os.getenv("BOT_TOKEN"))

# <Message id=1044146152364314655 channel=<TextChannel id=633319703158325261 name='general' position=0 nsfw=False news=False category_id=633319703158325259> type=<MessageType.default: 0> author=<Member id=633300276626980864 name='shayon' discriminator='9352' bot=False nick=None guild=<Guild id=633319702458007586 name='shayon server' shard_id=0 chunked=False member_count=5>> flags=<MessageFlags value=0>>
