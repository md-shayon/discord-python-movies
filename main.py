# Example - https://discordpy.readthedocs.io/en/stable/quickstart.html
import discord
import threading
from discord.ext import commands
import os
import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from FetchMovie import FetchMovie
from Helper import Helper

load_dotenv()  # take environment variables from .env.
intents = discord.Intents.default()
intents.message_content = True

# Connection to discord
client = discord.Client(intents=intents)

app = FastAPI()
f_movie = FetchMovie()
helper = Helper()


# register an event
@client.event
async def on_ready():
    print("We have logged in as {0.user}".format(client))


# all the message we receive
@client.event
async def on_message(message):
    # print(message)
    # We do not want to do anything if the message is from bot itself
    if message.author == client.user:
        return

    msg = message.content

    # Commands from the bot
    if msg.startswith("/movie"):
        # find movie (text after /movie) name with regular expression
        movie_name = helper.get_movie_name(msg)

        """
        Scrape data from site 1
        And arrange that data to python dictionary using beautiful soup
        ================================================================================================================ 
        """
        movie_name_dash = helper.format_movie_name_with_dash(movie_name=movie_name)
        url = "{0}/video/{1}".format(f_movie.site1_base_url, movie_name_dash)
        card_element = f_movie.get_individual_movie_site1(url)
        if card_element is None:
            keyword = helper.format_movie_keyword_with_plus(movie_name)
            url = "{0}/search?keyword={1}".format(f_movie.site1_base_url, keyword)
            search_movie_list = f_movie.get_movie_list_from_site1(url)
            title_list = search_movie_list.findAll("a", title=True)
            new_title_list = []
            selected_element = None
            for i in range(0, len(title_list)):
                new_title_list.append(title_list[i])
                if movie_name.lower() in title_list[i].text.lower() or title_list[i].text.lower() in movie_name.lower():
                    selected_element = title_list[i]
                    break
                elif movie_name.lower() in title_list[i].text or title_list[i].text.split("(")[0].lower() in movie_name.lower():
                    selected_element = title_list[i]
                    break
                elif movie_name.lower() in title_list[i].text or title_list[i].text.split(":")[0].lower() in movie_name.lower():
                    selected_element = title_list[i]
                    break



            if selected_element:
                # search with selected_element
                url = selected_element.attrs['href']
                card_element = f_movie.get_individual_movie_site1(url)
                content = f_movie.format_movie_card_detail(card_element)
                embed_site1 = helper.movie_embed_element(content, discord)
                await message.channel.send(url, embed=embed_site1)
            else:
                # return not found result
                embed_site1 = helper.movie_embed_nf_element(discord)
                await message.channel.send(url, embed=embed_site1)
        else:
            content = f_movie.format_movie_card_detail(card_element)
            # print(content)

            # if content is empty use default not found
            embed_site1 = helper.movie_embed_element(content, discord)
            await message.channel.send(url, embed=embed_site1)

        """
        Scrape data from site 2
        And arrange that data to python dictionary using beautiful soup
        ================================================================================================================ 
        """


        # await message.channel.send(url, embed=embed)



# We are going to make requests to this api endpoint
@app.get("/search-movie", response_class=HTMLResponse)
async def search_movie():
    url = "https://flixtor.to/show/search/black%20adam"
    result = f_movie.get_movie_list_from_site2_no_proxy(url)
    return result

@app.get("/search-movie-proxy", response_class=HTMLResponse)
async def search_movie_proxy():
    url = "https://flixtor.to/show/search/black%20adam"
    result = f_movie.get_movie_list_from_site2(url)
    return result

@app.get("/test")
async def root():
    return {"msg": "Hello World"}

def run_server():
    uvicorn.run("main:app", host=os.getenv("HOST"), port=int(os.getenv("PORT")), log_level="info")

def run_discord_bot():
    # print(embed)

    # if there is no individual movie list of movie and match with word
    # f_movie.get_movie_list_from_site1(movie_name=movie_name)
    client.run(os.getenv("BOT_TOKEN"))


if __name__ == "__main__":
    t1 = threading.Thread(target=run_discord_bot, name="bot")
    t2 = threading.Thread(target=run_server, name="server")

    # starting threads
    t1.start()
    t2.start()

    # wait until all threads finish
    t1.join()
    t2.join()







