import requests
import cloudscraper
import re
import os
from bs4 import BeautifulSoup
from time import sleep


# https://oxylabs.io/blog/python-web-scraping
# https://realpython.com/beautiful-soup-web-scraper-python/

class FetchMovie():
    def __init__(self):
        self.site1_base_url = "https://freetubespot.com"
        self.site2_base_url = "https://flixtor.to"

    """
    Site 1
    ====================================================
    """
    def format_movie_card_detail(self, html_item):
        content = {}
        try:
            thumbnail_href = html_item.find('img', class_="img-polaroid thumbnail")
            content["thumbnail"] = thumbnail_href.attrs['src']

            # print(thumbnail_href.attrs['src'])

            detail_content = html_item.find("div", class_="span6 detail")

            title = detail_content.find("a")
            content['title'] = title.text

            # soup.find(lambda tag: tag.name == "a" and "Edit" in tag.text)
            release = detail_content.find(lambda tag: tag.name == "p" and "Release" in tag.text)
            content["release"] = release.text.split(" ")[1]

            genre = detail_content.find(lambda tag: tag.name == "p" and "Genre" in tag.text)
            all_genre = genre.findAll('a')
            genre_text = ""
            for i in range(0, len(all_genre)):
                if i != len(all_genre) - 1:
                    genre_text += f"{all_genre[i].text}, "
                else:
                    genre_text += all_genre[i].text
            content['genre'] = genre_text

            likes = detail_content.find('button', "rate-like btn btn-mini").find('b')
            content['likes'] = likes.text

            dislikes = detail_content.find('button', "rate-dislike btn btn-mini").find('b')
            content['dislikes'] = dislikes.text

            detail = detail_content.find(lambda tag: tag.name == "p" and "Director" in tag.text)
            content["detail"] = detail.text

            return content

        except Exception as e:
            print(e)

        return content

    def get_individual_movie_site1(self, url):
        # https://freetubespot.com/video/Blues-Big-City-Adventure
        try:
            sleep(5)
            response = requests.get(url)
            # print(response.text)
            soup = BeautifulSoup(response.text, "html.parser")
            # print(soup.find("div", class_="item"))
            item = soup.find("div", class_="item")
            return item
        except Exception as e:
            print(e)
        return None

    def get_movie_list_from_site1(self, url):
        try:
            # print(movie_name)
            #  Url concatenation
            # Blue's Big City Adventure
            response = requests.get(url)
            soup = BeautifulSoup(response.text, 'html.parser')
            # print(soup)
            table_element = soup.find('table', class_="table table-striped")
            # print(response.text)
            return table_element
        except Exception as e:
            print(e)
        return None


    """
    Site 2
    ====================================================
    """
    def get_movie_list_from_site2(self, url):
        try:

            # proxy = "92.205.22.114:38080"
            # proxy = "122.49.208.242:3128"
            # url = "https://flixtor.to/show/search/black%20adam/from/1900/to/2099/rating/0/votes/0/language/all/type/all/genre/all/relevance/page/1"
            # url = "https://flixtor.to"
            response = requests.get(url, proxies={"http": os.getenv("PROXY"), "https": os.getenv("PROXY")}, timeout=4)
            # soup = BeautifulSoup(response.text, 'html.parser')
            # content_element = soup.find('div', class_="contentList")
            # print(response)
            return response.text
        except Exception as e:
            # print(e)
            pass
        return None

    def get_movie_list_from_site2_no_proxy(self, url):
        try:
            response = requests.get(url, timeout=4)
            # soup = BeautifulSoup(response.text, 'html.parser')
            # content_element = soup.find('div', class_="contentList")
            # print(response)
            return response.text
        except Exception as e:
            # print(e)
            pass
        return None
