import requests
from bs4 import BeautifulSoup

# get proxies https://free-proxy-list.net/
# try:
#     # proxy = "92.205.22.114:38080"
#     proxy = "122.49.208.242:3128"
#     # url = "https://flixtor.to/show/search/black%20adam/from/1900/to/2099/rating/0/votes/0/language/all/type/all/genre/all/relevance/page/1"
#     url = "https://flixtor.to"
#     response = requests.get(url, proxies={"http": proxy, "https": proxy}, timeout=4, verify=True)
#     soup = BeautifulSoup(response.text, 'html.parser')
#     # print(soup.find("div", class_="contentList"))
#     print(soup)
# except Exception as e:
#     print(e)


import cloudscraper

proxy = "92.205.22.114:38080"
proxy = "122.49.208.242:3128"
scraper = cloudscraper.create_scraper()
response = scraper.get("https://flixtor.to", proxies={"http": proxy, "https": proxy})
print(response.text)