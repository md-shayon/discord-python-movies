# Discord bot
 - [Tutorial 1](https://www.youtube.com/watch?v=SPTfmiYiuok), [Tutorial 2](https://www.youtube.com/watch?v=pbLDkWk6f_E), [tutorial 3](https://www.youtube.com/watch?v=NdXnE1Adk1M)

### Setup discord
 1. Create a discord server and a discord bot account
 2. Create a new bot account from [discord developer portal](https://discord.com/developers/applications) -> [bot tab](https://discord.com/developers/applications/<1044130780781219870>/bot) -> build the bot
 3. From bot tab -> enable **PRESENCE INTENT**, **SERVER MEMBERS INTENT**, **MESSAGE CONTENT INTENT**
 4. Copy bot token
 5. Now invite the bot to our server -> [Oauth2 tab](https://discord.com/developers/applications/<1044130780781219870>/oauth2/url-generator) -> click on bot -> from below get all permission we need with bot -> now copy the url -> paste it in browser -> authorize with your server
 6. by default bot will be offline -> we have to make it online using python code

### Discord.py
 - [docs](https://discordpy.readthedocs.io/en/stable/), [Example](https://discordpy.readthedocs.io/en/stable/quickstart.html)
 - It revolves around concept of events and event is something we listen to and respond to 

### Deployment
 - Using https://replit.com
 - Create flask or fast api server (by default it will run an hour afterward it will be in sleep mode)
 - Using **UptimeRobot** make request in every 5 minutes that way server will run continuously
 - https://dash.daki.cc/
 - **Deployment using daki**
 - go to __https://dash.daki.cc/__ and login with discord
 - Create server -> 

### Instructions
 - https://freetubespot.com/
 - https://flixtor.to/
 - When someone type /movie moviename Get a box of information like below
 - Make private bot
 - Host this bot with FastAPI

### Request with proxies
 - Get available free proxy https://free-proxy-list.net


### Answer stackoverflow questions
 - https://stackoverflow.com/questions/31958637/beautifulsoup-search-by-text-inside-a-tag
 - https://stackoverflow.com/questions/44862112/how-can-i-send-an-embed-via-my-discord-bot-w-python
 - https://stackoverflow.com/questions/23414369/what-is-a-beautiful-soup-bound-method
 - https://stackoverflow.com/questions/5015483/test-if-an-attribute-is-present-in-a-tag-in-beautifulsoup
 - https://www.scrapingbee.com/blog/python-requests-proxy/